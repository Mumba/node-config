/**
 * @copyright 2015 Mumba Pty Ltd. All rights reserved.
 * @license   Apache-2.0
 */

/**
 * JSON formatter.
 *
 * @constructor
 */
export class JsonFormatter {
	/**
	 * Convert a JSON string into an object.
	 *
	 * @param {String} source
	 * @return {Object}
	 */
	public parse = function (source: string) {
		return JSON.parse(source);
	};
}
