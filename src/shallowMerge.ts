/**
 * Config package
 *
 * @copyright 2015 Mumba Pty Ltd. All rights reserved.
 * @license   Apache-2.0
 */

/**
 * Determine if the value is a leaf value.
 *
 * @param value
 * @returns {boolean}
 */
function isLeaf(value: any) {
	return value === undefined || value === null || value.constructor !== Object;
}

/**
 * Perform a shallow merge of one object with another.
 *
 * @param {Object} parent
 * @param {Object} object - The object to merge into the parent.
 */
export var shallowMerge = function shallowMerge(parent: any, object: any) {
	// Ignore an non-object.
	if (isLeaf(object)) {
		return;
	}

	Object.keys(object)
		.forEach(function (key) {
			// Check if the value of the key is not a native object.
			if (isLeaf(object[key])) {
				parent[key] = object[key];
			}
			// A native object means we have a new level.
			else {
				// Merge the object with the next level (creating that level if it doesn't exist.
				shallowMerge(parent[key] || (parent[key] = {}), object[key]);
			}
		});
};
