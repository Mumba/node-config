/**
 * @copyright 2015 Mumba Pty Ltd. All rights reserved.
 * @license   Apache-2.0
 */

var fs = require('fs');
var path = require('path');

import {Loader} from "../Config";
import {JsonFormatter} from "../formats/Json";
var jsonFormatter = new JsonFormatter();

export interface FileLoaderOptions {
	filePath: string
}

/**
 * File reader.
 *
 * @constructor
 */
export class FileLoader implements Loader {
	/**
	 * Get the unique name of the loader.
	 *
	 * @returns {string}
	 */
	public getName() {
		return 'file';
	}

	/**
	 * Load a file, running the appropriate format based on the extension.
	 *
	 * @param {String} path
	 * @return {Object}
	 */
	public load(options: FileLoaderOptions): Promise<any> {
		var ext = path.extname(options.filePath);

		if (ext === '.js') {
			let content = require(options.filePath);

			if (content.__esModule) {
				content = content.default;
			}

			return Promise.resolve(content);
		}
		else {
			try {
				// TODO use async file reader here.
				let parsed = jsonFormatter.parse(fs.readFileSync(options.filePath));

				return Promise.resolve(parsed);
			}
			catch (e) {
				return Promise.reject(new Error('FileLoader.load: ' + e.message + ' in file ' + options.filePath));
			}
		}
	};
}
