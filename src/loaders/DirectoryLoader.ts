/**
 * @copyright 2015 Mumba Pty Ltd. All rights reserved.
 * @license   Apache-2.0
 */

var fs = require('fs');
var path = require('path');

import {Loader} from "../Config";
import {shallowMerge} from "../shallowMerge";
import {FileLoader} from "./FileLoader";

let fileReader = new FileLoader();

var supportedFileTypes: any = {
	'.js': true,
	'.json': true
};

export interface DirectoryLoaderOptions {
	dirPath: string,
	exclude?: RegExp
}

/**
 * Directory reader.
 *
 * @constructor
 */
export class DirectoryLoader implements Loader {
	/**
	 * Get the unique name of the loader.
	 *
	 * @returns {string}
	 */
	public getName() {
		return 'directory';
	}

	/**
	 * Load a directory, running the appropriate formatters.
	 *
	 * @param {String} dirPath
	 * @param {RegExp}  [exclude]
	 * @returns {*}
	 */
	public load(options: DirectoryLoaderOptions): Promise<any> {
		let merged = {};
		let promises: Promise<any>[] = [];

		fs.readdirSync(options.dirPath)
			.filter(function (filePath: string) {
				// Include only the supported file types.
				var ext = path.extname(filePath);

				return supportedFileTypes[ext];
			})
			.filter(function (filePath: string) {
				// Exclude a mask.
				return options.exclude ? !filePath.match(options.exclude) : true;
			})
			.forEach(function (filePath: string) {
				let promise = fileReader.load({
					filePath: path.join(options.dirPath, filePath)
				})
					.then((result: any) => {
						shallowMerge(merged, result);
					});

				promises.push(promise);
			});

		return Promise.all(promises)
			.then(() => merged);
	};
}
