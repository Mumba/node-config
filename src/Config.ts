/**
 * @copyright 2015 Mumba Pty Ltd. All rights reserved.
 * @license   Apache-2.0
 */

"use strict";

import {shallowMerge} from "./shallowMerge";

/**
 * Separate a path into its keys.
 *
 * @param {String} path
 * @param {String} separator - Default: ':'.
 * @returns {Array}
 */
function separatePath(path: string, separator: string = ':') {
	return (!path || !path.split) ? [] : path.split(separator);
}

/**
 * Build an object from an array of keys and a final value for the leaf key.
 *
 * @param {Array} keys
 * @param {*}     value
 * @returns {Object}
 */
function buildObject(keys: any[], value: any) {
	var result = {};
	var lastKey = keys.pop();

	var leaf = keys.reduce(function (previous, current) {
		// Create a new node and return it so the next key can be added to it.
		return (previous[current] = {});
	}, result);

	leaf[lastKey] = value;

	return result;
}

export interface Loader {
	getName(): string;
	load(options: any): Promise<any>;
}

/**
 * Config class.
 *
 * @constructor
 */
export class Config {
	private _store: any = {};
	private loaders: any = {};

	/**
	 * Constructor
	 *
	 * @param {object} [object] - Optional object to merge into the configuration at construction time.
	 */
	constructor(object?: any) {
		if (object) {
			this.merge(object);
		}
	}

	/**
	 * Add a loader.
	 *
	 * @param {Loader} loader
	 * @returns {Config}
	 */
	public registerLoader(loader: Loader): this {
		this.loaders[loader.getName()] = loader;

		return this;
	}

	/**
	 * Get a loader by name.
	 *
	 * @param {string} name
	 * @returns {Loader}
	 */
	private getLoader(name: string): Loader {
		return this.loaders[name]
	}

	/**
	 * Load data using loader configurations.
	 *
	 * @param {any[]} configs
	 * @returns {Promise<void>}
	 */
	public load(configs: any[]): Promise<void> {
		let promises: Promise<any>[] = [];

		configs.forEach((config: any) => {
			let loader = this.getLoader(config.name);

			if (!loader) {
				return Promise.reject(new Error('Config.getLoader: loader "' + config.name + '" not found'));
			}

			promises.push(this.getLoader(config.name).load(config));
		});

		return Promise.all(promises)
			.then((results: any[]) => {
				results.forEach(this.merge.bind(this));
			});
	}

	/**
	 * Get a value by path.
	 *
	 * Inspired by nconf.
	 *
	 * @param {String} [path]
	 * @return {Object}
	 * @see {@link https://github.com/indexzero/nconf/blob/af0e9fb7e7a36d51e6d43764b7954d706eac85cd/lib/nconf/stores/memory.js#L37}
	 */
	public get(path?: string) {
		var keys = separatePath(path);
		var target = this._store;
		var key: string;

		while (keys.length > 0) {
			key = keys.shift();

			if (target && target.hasOwnProperty(key)) {
				target = target[key];
				continue;
			}

			return undefined;
		}

		return target;
	};

	/**
	 * Set a value by path.
	 *
	 * @param {string} path
	 * @param {any} value
	 * @return {Config} this
	 */
	public set(path: string, value: any) {
		var keys = separatePath(path);

		if (keys.length > 0) {
			shallowMerge(this._store, buildObject(keys, value));
		}

		return this;
	};

	/**
	 * Merge an object into the configuration.
	 *
	 * @param {Object} [object]
	 * @returns {Config}
	 */
	public merge(object?: any) {
		shallowMerge(this._store, object);

		return this;
	};
}
