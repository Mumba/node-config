# Mumba Config

A very simple configuration handler in the tradition of `nconf`.

## Installation

```sh
$ npm install mumba-config
```

## Examples

```typescript
import {Config} from "mumba-config";

let config = new Config({
  db: {
    user: 'user',
    pass: 'password'
  }
});

config.get();          // { db: { user: 'user', pass: 'password' } }
config.get('db');      // { user: 'user', pass: 'password' }
config.get('db:user'); // 'user'
config.get('db:pass'); // 'password'

config.set('log:level', 'silly');
```

Loading a file or directory

```typescript
import {Config, FileLoader, DirectoryLoader} from "mumba-config";

let config = new Config();

config.registerLoader(new FileLoader())
	.registerLoader(new DirectoryLoader());

config.load([{
		name: 'file',
		filePath: '/path/to/file.json'
	},
	{
		name: 'directory',
		dirPath: 'path/to/dir',
		exclude: 'local\.js'
	}])
	.then(() => {
		console.log(config.get())
	})
	.catch(console.error);
```

## Loaders

### File

Loads a JavaScript or JSON configuration file.

Parameter | Type | Description
--- | :---: | ---
`type` | `string` | **Required** Must be set to `file`.
`filePath` | `string` | **Required** The full path to the configuration file to load.

### Directory

Loads a directory of JavaScript or JSON files.

Parameter | Type | Description
--- | :---: | ---
`type` | `string` | **Required** Must be set to `file`.
`dirPath` | `string` | **Required** The full path to directory containing the configuration files.
`exclude` | `string` | An optional regular expression for files to exclude from the directory.

## Tests

To run the test suite, first install the dependencies, then run `npm test`:

```sh
$ npm install
$ npm test
```

## People

The original author of _Mumba Config_ is [Andrew Eddie](https://gitlab.com/u/aeddie.mumba).

## License

[Apache 2.0](LICENSE.txt)

* * *

&copy; 2016 [Mumba Pty Ltd](http://www.mumba.cloud). All rights reserved.

