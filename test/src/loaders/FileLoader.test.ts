/**
 * FileReader tests.
 *
 * Remember to observe the 4 A's of testing and try to limit tests to just four calls:
 * - Arrange    - set up the system state
 * - Act        - do the thing we are testing
 * - Assert     - inspect the resulting state
 * - Annihilate - tear down
 *
 * @copyright 2016 Mumba Pty Ltd. All rights reserved.
 * @license   Apache-2.0
 */

import * as assert from "assert";
import {FileLoader} from "../../../src/index";

const __fixtures = require('app-root-path') + '/test/_fixtures';

describe('loaders/File', function () {
	let instance: FileLoader;

	beforeEach(() => {
		instance = new FileLoader();
	});

	it('should get the name of the loader', () => {
		assert.equal(instance.getName(), 'file', 'should get the name');
	});

	it('should read a JS file', function () {
		let instance = new FileLoader();

		return instance.load({
				filePath: __fixtures + '/test.js'
			})
			.then((result: any) => {
				assert.deepEqual(result, {
					js: true
				});
			});
	});

	it('should read an ES6-style JS file', function () {
		let instance = new FileLoader();

		return instance.load({
				filePath: __fixtures + '/es6.js'
			})
			.then((result: any) => {
				assert.strictEqual(result.es6, true);
			});
	});

	it('should read a JSON file', function () {
		let instance = new FileLoader();
		
		return instance.load({
				filePath: __fixtures + '/test.json'
			})
			.then((result: any) => {
				assert.deepEqual(result, {
					json: true
				});
			});
	});

	it('should error with information if JSON parsing fails', function (done) {
		let instance = new FileLoader();

		return instance.load({
				filePath: require('app-root-path') + '/test/src/loaders/bad.json'
			})
			.catch((err: Error) => {
				assert(/Unexpected token #/.test(err.message));
				done();
			})
			.catch(done);
	});
});
