/**
 * DirectoryReader tests.
 *
 * Remember to observe the 4 A's of testing and try to limit tests to just four calls:
 * - Arrange    - set up the system state
 * - Act        - do the thing we are testing
 * - Assert     - inspect the resulting state
 * - Annihilate - tear down
 *
 * @copyright 2016 Mumba Pty Ltd. All rights reserved.
 * @license   Apache-2.0
 */

import * as assert from "assert";
import {DirectoryLoader} from "../../../src/index";

const __fixtures = require('app-root-path') + '/test/_fixtures';

describe('loaders/Directory', function () {
	let instance: DirectoryLoader;

	beforeEach(() => {
		instance = new DirectoryLoader();
	});

	it('should get the name of the loader', () => {
		assert.equal(instance.getName(), 'directory', 'should get the name');
	});

	it('should load a directory', function () {
		return instance.load({
				dirPath: __fixtures
			})
			.then((result: any) => {
				assert.deepEqual(result, {
					local: true,
					js: true,
					es6: true,
					json: true
				});
			});
	});

	it('should load a directory and exclude a regex', function () {
		return instance.load({
				dirPath: __fixtures,
				exclude: /local|es6/
			})
			.then((result: any) => {
				assert.deepEqual(result, {
					js: true,
					json: true
				});
			});
	});
});
