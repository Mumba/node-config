/**
 * JsonFormatter tests.
 *
 * Remember to observe the 4 A's of testing and try to limit tests to just four calls:
 * - Arrange    - set up the system state
 * - Act        - do the thing we are testing
 * - Assert     - inspect the resulting state
 * - Annihilate - tear down
 *
 * @copyright 2016 Mumba Pty Ltd. All rights reserved.
 * @license   Apache-2.0
 */

import * as assert from "assert";
import {JsonFormatter} from "../../../src/formats/Json";

describe('formats/JSON', function () {
	var instance = new JsonFormatter();

	it('should parse a JSON string', function () {
		assert.deepEqual(instance.parse('{ "foo": { "bar": true } }'), {
			foo: {
				bar: true
			}
		});
	});
});
