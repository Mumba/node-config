/**
 * Config tests.
 *
 * Remember to observe the 4 A's of testing and try to limit tests to just four calls:
 * - Arrange    - set up the system state
 * - Act        - do the thing we are testing
 * - Assert     - inspect the resulting state
 * - Annihilate - tear down
 *
 * @copyright 2016 Mumba Pty Ltd. All rights reserved.
 * @license   Apache-2.0
 */

import * as assert from "assert";
import {Config} from "../../src/Config";
import {FileLoader} from "../../src/loaders/FileLoader";
import {DirectoryLoader} from "../../src/loaders/DirectoryLoader";

const __fixtures = require('app-root-path') + '/test/_fixtures';

describe('Config', function () {
	var instance: Config;

	beforeEach(function () {
		instance = new Config();
	});

	it('should add a loader', () => {
		let loader = new FileLoader();

		assert.strictEqual(instance.registerLoader(loader), instance, 'should chain');
	});

	it('should throw if loader not found', (done) => {
		instance.load([{
			name: 'foo'
		}])
			.then(done)
			.catch((err: Error) => {
				assert(/loader "foo" not found/.test(err.message));
			})
			.catch(done);
	});

	it('should load configuration via loader configuration (in order)', () => {
		instance.registerLoader(new DirectoryLoader())
			.registerLoader(new FileLoader());

		return instance.load([
			{
				name: 'directory',
				dirPath: __fixtures,
				exclude: /local/
			},
			{
				name: 'file',
				filePath: __fixtures + '/local.js'
			}
		])
	});

	it('should do nothing if load is called with an empty array', () => {
		return instance.load([]);
	});

	it('should set and get a value', function () {
		assert.strictEqual(instance.set('foo:bar', 'yes').get('foo:bar'), 'yes');
	});

	it('should get the entire store by passing nothing to get', function () {
		instance.set('test', true);
		assert.deepEqual(instance.get(), { test: true });
	});

	it('should ignore an unprocessable path', function () {
		assert.deepEqual(instance.set(null, 'yes').get(), {});
	});

	it('should maintain deep references that can be altered', function () {
		instance.set('a:b:c', '*a.b.c');
		instance.set('a:b:d', '*a.b.d');

		var ref = instance.get('a:b');

		assert.deepEqual(ref, {
			c: '*a.b.c',
			d: '*a.b.d'
		});

		instance.set('a:b:d', '*a.b.d2');

		assert.equal(ref.d, '*a.b.d2', 'should overwrite everywhere');
	});

	it('should merge another object en-masse', function () {
		var object = new Buffer('complex object');
		var array = ['an array'];
		var date = new Date();

		return instance.registerLoader(new DirectoryLoader())
			.load([{
				name: 'directory',
				dirPath: __fixtures,
				exclude: /local|es6/}])
			.then(() => {
				instance.merge({
					a: {
						array: array,
						object: object,
						date: date,
						undefined: undefined,
						null: null,
						0: 0
					}
				});

				assert.deepEqual(instance.get(), {
					js: true,
					json: true,
					a: {
						array: array,
						object: object,
						date: date,
						undefined: undefined,
						null: null,
						0: 0
					}
				});

				assert(instance.get('a:array') === array, 'should not clone the array');
				assert(instance.get('a:object') === object, 'should not clone the object');
				assert(instance.get('a:date') === date, 'should not clone the date');
			});
	});

	it('should ignore merging a non-object or an empty object', function () {
		var original = JSON.stringify(instance.get());

		instance.merge();
		instance.merge(null);
		instance.merge(undefined);
		instance.merge(true);
		instance.merge(new Buffer('ignore me'));
		instance.merge('a string');
		instance.merge({});

		assert.equal(original, JSON.stringify(instance.get()), 'should have ignored all the invalid inputs without error');
	});

	it('should construct with an optional object', () => {
		let newInstance = new Config({
			foo: 'bar'
		});

		assert.equal(newInstance.get('foo'), 'bar');
	});
});
